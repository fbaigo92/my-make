# Makefile for module compilation
# Must be allocated in /src.

###### Edit here ######

# Module include path, only one path.
INC_APP_PATH := ./app/inc
# Module source path, only one path. 
SRC_APP_PATH := ./app/src

###### Don't edit ######
# Compilation source path list
SRC_PATH += $(SRC_APP_PATH)
# Module source files
SRC_APP_FILES := $(wildcard $(SRC_APP_PATH)/*.c)
SRC_FILES += $(SRC_APP_FILES)
# Include path for compilation
INC_PATH += $(INC_APP_PATH)
# Module object files
OBJ_APP_FILES := $(notdir $(SRC_APP_FILES:.c=.o))
OBJ_FILES += $(OBJ_APP_FILES)
