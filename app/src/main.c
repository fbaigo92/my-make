#include <stdio.h>
#include <stdint.h>
#include "../../prom/inc/prom.h"
#include "../../calc/inc/calc.h"

int main (void){
	uint32_t my_serie[] = {1,2,3,4,5,6};
	
	printf("Calculating average ...\n");
	printf("Average is: %d\n", promd(my_serie,6));
	
	printf("Calculating sum of %d and %d ... \n", my_serie[0], my_serie[1]);	printf("Sum is: %d", sum(my_serie[0], my_serie[1]));	
	return 0;
}
