#ifndef __CALC_H__
#define __CALC_H__

uint32_t sum(uint8_t, uint8_t);
uint32_t diff(uint8_t, uint8_t);

#endif
