# Makefile for module compilation
# Must be allocated in /src.

###### Edit here ######

# Module include path, only one path.
INC_CALC_PATH := ./calc/inc
# Module source path, only one path. 
SRC_CALC_PATH := ./calc/src

###### Don't edit ######

# Compilation source path list
SRC_PATH += $(SRC_CALC_PATH)
# Module source files
SRC_CALC_FILES := $(wildcard $(SRC_CALC_PATH)/*.c)
SRC_FILES += $(SRC_CALC_FILES)
# Include path for compilation
INC_PATH += $(INC_CALC_PATH)
# Module object files
OBJ_CALC_FILES := $(notdir $(SRC_CALC_FILES:.c=.o))
OBJ_FILES += $(OBJ_CALC_FILES)
