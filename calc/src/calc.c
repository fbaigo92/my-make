#include <stdint.h>

uint32_t sum(uint8_t aterm, uint8_t bterm){
	return aterm + bterm;
}

uint32_t diff(uint8_t aterm, uint8_t bterm){

	if(aterm < bterm)
		return bterm - aterm;
	else 
		return aterm - bterm;
}
