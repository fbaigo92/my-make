# Author: Federico Baigorria
# Title: Project makefile
# Manage and include module makefiles for compilation and binary generation
#
# References
# Grubb's style guide: https://clarkgrubb.com/makefile-style-guide#phony-targets
# P. Ridolfi examples: https://github.com/pridolfi/learning_make
# GNU: https://www.gnu.org/software/make/manual/html_node/Text-Functions.html


###### Edit here ######

# Verbose mode. Y/N
VERBOSE := Y

# Makefile modules needed
include ./calc/src/calc.mk
include ./prom/src/prom.mk
include ./app/src/app.mk

# Compilation object project path. Only one path
# Don't forget the last /
OBJ_PATH = ./out/

###### Don't edit ######

# Compilation dependencies
DEP_FILES = $(OBJ_FILES:.o=.d)
# Application name
EXE = $(notdir $(shell pwd))
# Append the -I to include paths
INC_GCC = $(addprefix -I, $(INC_PATH))

# Directory path names for search
vpath %.o $(OBJ_PATH)
vpath %.c $(SRC_PATH)

# Modules compilation
%.o: %.c
ifeq ($(VERBOSE),Y)
	@echo "\033[1;33mCompiling $< file...\033[0m"
	gcc $(INC_GCC) -c $< -o $(OBJ_PATH)$@
	@echo "\033[1;33mBuilding dependence $<...\033[0m"
	gcc $(INC_GCC) -MM $< > $(OBJ_PATH)$(@:.o=.d)
else
	@gcc $(INC_GCC) -c $< -o $(OBJ_PATH)$@
	@gcc $(INC_GCC) -MM $< > $(OBJ_PATH)$(@:.o=.d)
endif

# Binary compilation
-include $(DEP_FILES)

$(EXE): $(OBJ_FILES)
ifeq ($(VERBOSE),Y)
	@echo "\033[1;33mLinking $(EXE)...\033[0m"
	gcc $(patsubst %, $(OBJ_PATH)%, $(OBJ_FILES)) -o ./$(EXE)
else
	@gcc $(patsubst %, $(OBJ_PATH)%, $(OBJ_FILES)) -o ./$(EXE)
endif

# Clean
.PHONY: clean
clean:
	cd $(OBJ_PATH); rm -f $(OBJ_FILES) $(DEP_FILES);

# Debuging information
.PHONY: debug
debug:
	@echo EXE: $(EXE)
	@echo INCS: $(INC_PATH)
	@echo SRC_PATH: $(SRC_PATH)
	@echo SRC_FILES: $(SRC_FILES)
	@echo OBJ_PATH: $(OBJ_PATH)
	@echo OBJ_FILES: $(OBJ_FILES)
	@echo DEP_FILES: $(DEP_FILES)

# Makefile usage
.PHONY: help
help:
	@echo "For making the executable binary do: make"
	@echo "For debugging do: make debug"
	@echo "For cleaning do: make clean"

