// Dependencies
#include <stdint.h>

// Public functions
uint32_t promd(uint32_t *serie, uint8_t size){
	uint8_t i;
	uint32_t sum = 0;

	for(i=0; i<size; i++){
		sum += *(serie + i);}

	return sum / size;
}
