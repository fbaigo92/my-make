# Makefile for module compilation
# Must be allocated in /src.

###### Edit here ######

# Module include path, only one path.
INC_PROM_PATH := ./prom/inc
# Module source path, only one path.
SRC_PROM_PATH := ./prom/src

###### Don't edit ######

# Compilation source path list
SRC_PATH += $(SRC_PROM_PATH)
# Module source files
SRC_PROM_FILES := $(wildcard $(SRC_PROM_PATH)/*.c)
SRC_FILES += $(SRC_PROM_FILES) 
# Include path for compilation
INC_PATH += $(INC_PROM_PATH)
# Module object files
OBJ_PROM_FILES := $(notdir $(SRC_PROM_FILES:.c=.o))
OBJ_FILES += $(OBJ_PROM_FILES)
